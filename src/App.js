import React, { useState, useEffect } from 'react'

const data = [
  {
    label: 'Phone',
    value: '+7 888 558 88 88'
  },
  {
    label: 'Adress',
    value: 'Moscow, Nikitskaya 1'
  }
]



function App() {
  const [entries, setEntries] = useState(data)
  const [newEntry, setNewEntry] = useState({})

  useEffect(() => {
    entries.push(newEntry)
    setEntries(entries)
  }, [newEntry])


  return (
    <div className='layout'>
      <div className='check-view'>

          <div className='check-letter'>
            <div className='check-logo'>
              <img className='check-logo-img' src='assets/logo.svg' alt=''/>
            </div>
            <div className='check-divider'></div>
            <div className='check-entry entry-columns'>
              <div className='entry-label'>Товар 1</div>
              <div className='entry-right'>100 ₽</div>
            </div>
            <div className='check-divider'></div>
            <div className='check-entry entry-columns'>
              <div className='entry-label'>Товар 2</div>
              <div className='entry-right'>250 ₽</div>
            </div>
            <div className='check-divider'></div>
            <div className='check-entry entry-columns'>
              <div className='entry-label'>Товар 3</div>
              <div className='entry-right'>1200 ₽</div>
            </div>
            <div className='check-divider'></div>
            <div className='check-entry entry-columns'>
              <div className='entry-label'>ИТОГО</div>
              <div className='entry-right'>1550 ₽</div>
            </div>
            <div className='check-entry entry-qr'>
              <div className='entry-qr'>
                <img className='entry-qr-img' src='assets/logo.svg' alt=''/>
              </div>
            </div>
            <div className='check-divider'></div>
            <div className='check-entry'>
              <div className='entry-title'>Внимание!</div>
              <div>Текст вашего сообщения…</div>
            </div>
            <div className='check-divider'></div>
            <div className='check-entry entry-contacts'>
              <div className='entry-title'>Наши контакты</div>

              <div className='entry-contact'>
                <div className='entry-contact-label'>Телефон, Whats App, Telegram</div>
                <div className='entry-contact-value'>+79037461851</div>
              </div>

              <div className='entry-contact'>
                <div className='entry-contact-label'>Email</div>
                <div className='entry-contact-value'>info@holybed.ru</div>
              </div>
            </div>
            <div className='check-divider'></div>
            <div className='check-entry entry-site'>
              <div className='entry-title'>Наш сайт</div>
              <div className='entry-site-qr'>
                <img className='entry-site-qr-img' src='assets/logo.svg' alt=''/>
                <div className='entry-site-url'>holybed.ru</div>
              </div>
            </div>
          </div>

      </div>
      <div className='check-control'>

        <h1 className='check-control-caption'>БЛОКИ И ИХ ПОРЯДОК</h1>

        <div className='control-item'>

          <div className='control-actions'>
            <button className='action'>1</button>
            <button className='action'>2</button>
            <button className='action'>3</button>
          </div>

          <div className='control-item-title'>
            <div className='item-trigger'>
              <label className='trigger'>
                <input type='checkbox' value='' className='trigger-input'/>
                <div className='trigger-shape'></div>
                <span className='trigger-label'>Картинка / лого</span>
              </label>
            </div>
          </div>

          <div className='control-logo'>
            <img className='control-logo-img' src='assets/logo.svg' alt=''/>
            <button className='button'>Изменить</button>
          </div>
        </div>

        <div className='control-item'>

          <div className='control-actions'>
            <button className='action'>2</button>
            <button className='action'>3</button>
          </div>

          <div className='control-item-title'>Основной блок с товарами</div>

          <div className='item-trigger'>
            <label className='trigger'>
              <input type='checkbox' value='' className='trigger-input'/>
              <div className='trigger-shape'></div>
              <span className='trigger-label'>Разделители между товарами в основном блоке</span>
            </label>
          </div>


          <div className='item-trigger'>
            <label className='trigger'>
              <input type='checkbox' value='' className='trigger-input'/>
              <div className='trigger-shape'></div>
              <span className='trigger-label'>Показывать общую скидку на чек, сумму чека до применения
скидок и цены на товары до применения скидок</span>
            </label>
          </div>

          <div className='item-trigger'>
            <label className='trigger'>
              <input type='checkbox' value='' className='trigger-input'/>
              <div className='trigger-shape'></div>
              <span className='trigger-label'>Пунктирные разделители между блоками в чеке</span>
            </label>
          </div>

        </div>

        <div className='control-item'>

          <div className='control-actions'>
            <button className='action'>1</button>
            <button className='action'>2</button>
            <button className='action'>3</button>
          </div>

          <div className='control-item-title'>
            <div className='item-trigger'>
              <label className='trigger'>
                <input type='checkbox' value='' className='trigger-input'/>
                <div className='trigger-shape'></div>
                <span className='trigger-label'>Сообщения на чеке</span>
              </label>
            </div>
          </div>

          <label className='field'>
            <div className='field-label'>Заголовок секции (опционально)</div>
            <input className='field-input' type='text' value='Внимание!' />
          </label>

          <label className='field'>
            <div className='field-label'>Заголовок секции (опционально)</div>
            <textarea className='field-input'>Текст вашего сообщения…</textarea>
          </label>
        </div>


        <div className='control-item'>

          <div className='control-actions'>
            <button className='action'>1</button>
            <button className='action'>2</button>
            <button className='action'>3</button>
          </div>

          <div className='control-item-title'>
            <div className='item-trigger'>
              <label className='trigger'>
                <input type='checkbox' value='' className='trigger-input'/>
                <div className='trigger-shape'></div>
                <span className='trigger-label'>Контакты</span>
              </label>
            </div>
          </div>

          <label className='field'>
            <div className='field-label'>Подзаголовок секции (опционально)</div>
            <input className='field-input' type='text' value='Наши контакты' />
          </label>

          <label className='field'>
            <div className='control-actions'>
              <button className='action'>1</button>
              <button className='action'>2</button>
              <button className='action'>3</button>
            </div>
            <div className='field-label'>Телефон</div>
            <input className='field-input' name='phone' type='text' value='+79037461851'></input>
          </label>
          <div className='phone-relations'>
            <label className='checkbox'>
              <input type='checkbox' value='' className='checkbox-input'/>
              <div className='checkbox-shape'></div>
              <span className='checkbox-label'>Телефон</span>
            </label>

            <label className='checkbox'>
              <input type='checkbox' value='' className='checkbox-input'/>
              <div className='checkbox-shape'></div>
              <span className='checkbox-label'>Whats App</span>
            </label>

            <label className='checkbox'>
              <input type='checkbox' value='' className='checkbox-input'/>
              <div className='checkbox-shape'></div>
              <span className='checkbox-label'>Telegram</span>
            </label>

            <label className='checkbox'>
              <input type='checkbox' value='' className='checkbox-input'/>
              <div className='checkbox-shape'></div>
              <span className='checkbox-label'>Viber</span>
            </label>
          </div>

          <label className='field'>
            <div className='control-actions'>
              <button className='action'>1</button>
              <button className='action'>2</button>
              <button className='action'>3</button>
            </div>
            <div className='field-label'>Email</div>
            <input className='field-input' name='phone' type='text' value='info@holybed.ru'></input>
          </label>

          <label className='field'>
            <div className='control-actions'>
              <button className='action'>1</button>
              <button className='action'>2</button>
              <button className='action'>3</button>
            </div>
            <div className='field-label'>Адрес</div>
            <input className='field-input' name='phone' type='text' value='Ленинградское шоссе, 39 АС 2'></input>
          </label>

          <label className='field'>
            <div className='control-actions'>
              <button className='action'>1</button>
              <button className='action'>2</button>
              <button className='action'>3</button>
            </div>
            <div className='field-label'>Комментарий</div>
            <input className='field-input' name='phone' type='text' value='офис 13'></input>
          </label>

          <label className='field'>
            <div className='control-actions'>
              <button className='action'>1</button>
              <button className='action'>2</button>
              <button className='action'>3</button>
            </div>
            <div className='field-label'>Facebook</div>
            <input className='field-input' name='phone' type='text' value='@holybed.ru'></input>
          </label>

          <label className='field'>
            <div className='control-actions'>
              <button className='action'>1</button>
              <button className='action'>2</button>
              <button className='action'>3</button>
            </div>
            <div className='field-label'>Instagram</div>
            <input className='field-input' name='phone' type='text' value='@holybed.ru'></input>
          </label>

          <label className='field'>
            <div className='control-actions'>
              <button className='action'>1</button>
              <button className='action'>2</button>
              <button className='action'>3</button>
            </div>
            <div className='field-label'>VKontakte</div>
            <input className='field-input' name='phone' type='text' value='holybed'></input>
          </label>

          <button className='add'>Добавить блок</button>

        </div>


        <div className='control-item'>

          <div className='control-actions'>
            <button className='action'>1</button>
            <button className='action'>2</button>
            <button className='action'>3</button>
          </div>

          <div className='control-item-title'>
            <div className='item-trigger'>
              <label className='trigger'>
                <input type='checkbox' value='' className='trigger-input'/>
                <div className='trigger-shape'></div>
                <span className='trigger-label'>QR-код</span>
              </label>
            </div>
          </div>

          <label className='field'>
            <div className='field-label'>Заголовок секции</div>
            <input className='field-input' name='phone' type='text' value='Наш сайт'></input>
          </label>

          <label className='field'>
            <div className='field-label'>URL или значение поля с QR</div>
            <input className='field-input' name='phone' type='text' value='holybed.ru'></input>
          </label>
        </div>


        <div className='control-item'>
          <button className='add'>Добавить блок</button>
        </div>

      </div>
    </div>
  )
}

export default App
