import React from 'react'
import './styles.css'
import Entry from "../Entry"

function Contacts({ entries }) {
  return (
    <div>
      contacts
      { entries.map( entry => <Entry label={entry.label} value={entry.value}/> ) }
    </div>
  )
}

export default Contacts
