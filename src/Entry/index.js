import React from 'react'

function Entry(props) {
  return (
    <div>
      <div className='label'>{props.label}</div>
      <div className='value'>{props.value}</div>
    </div>
  );
}

export default Entry;
